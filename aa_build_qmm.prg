#Include LM.h

Local lcBuildFilename, lcServerFileSource, lcServerFileSourceBackupCopy, lcSourceProject
Local llError, lcDestination 

Do aa_Generate_Include_File.prg && This will create a couple of global vars that are used below

Do aa_Build_Business_Objects_File.prg && Combines all prg in /classes/wwlm into a single file
									  && named "BusinessObjects.fxp" which is referenced in the Project.
lcTimeStamp = Ttoc(Datetime(),1)
lcAppName = "QMM"
lcBuildFilename = StringFormat("{0}_{1}.exe", lcAppName, lcTimeStamp)

lcBuildFilenameWithPath = Addbs(BUILD_EXE_TO_PATH) + lcBuildFilename
lcProjectPath = "H:\Work\lm5_qmm"
lcSourceProject = Addbs(lcProjectPath) + 'QMM.pjx'


? ' '
? '----------------------------------------------------------'
? 'Building ' + lcBuildFilenameWithPath + " from " + lcSourceProject

Try
	Build Exe (lcBuildFilenameWithPath) From (lcSourceProject)
Catch
	llError = .t.
Finally
Endtry

If llError = .t.
	MessageBox('Could not build new source file to server. Perhaps the source EXE file is in use???', 0, 'Error')
	Return -2
EndIf

lcDestination = LM_APP_ROOT 

Run /N "h:\work\lm5\aa_copy_to_server.bat" &lcBuildFilenameWithPath &lcDestination
	

? ' '
? 'Done.'    