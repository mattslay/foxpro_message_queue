&& Modify for your environment
#INCLUDE "lm.h"

*-- See: https://bitbucket.org/mattslay/lm5_qmm


*-- Note: This is a sample launch/bootstrap program to get the Message Queue app running.
*-- You will need to customize certain parts of this code to make it work properly in your environment.

*-- Parameters
*---------------------------------------------------------------------------------------
*-- Pass "TEST" as first parameter to check and see if there are too many unprocess messages
*-- in the Queue and it will send an email if QMM does not appear to be running.

*-- NOTES:
*---------------------------------------------------------------------------------------
*-- 1. Use this PowerShell script to kill any open process on the server:
*--      Get-Process | Where-Object {$_.Path -like "*QMM_*"} | Stop-Process

*-- Note: The following parameters are optional, and can be used to test the Sql Server database to make sure it is working.


*-- Modify for your environment
#DEFINE SOURCE_ROOT  "H:\work\lm5_qmm\" 

Lparameters tcUser, tcPassword

Public lClock_in_use, lm_sec_level, lm_dir

Private poConfig

Local lcUser, lcPassword, llShowUpdateDialog
Local lcLocalDir, lcMainProcedureFile, lcYear
Local loForm as "QueueMonitorForm"
Local lcAsyncClass, lcBootstrap, lcConnectionString, lcDatabase, lcLabelCaption, lcServer
Local lcUserName
Local lcMessageProcessorClass
Local loCheckMessageQueue 
Local llCheckQMM

Clear
Set Classlib To 
Set Procedure To 
Set Path To

lcApp = Sys(16,1)
llExe = '.EXE' $ Upper(lcApp)

lcBootstrap = LM_SOURCE_PATH + "\prg\Bootstrap.prg" && Modify for your environment, if needed.
	
If !llExe		

	Try
		Cd 'H:\Work\lm5_qmm' && Modify for your environment
	Catch
		Cd '\\lmapp\lm5' && Modify for your environment
	EndTry

	Try
		Set Resource to H:\work\lm5\lm5_resource
	Catch
	EndTry

EndIf

*-- This is a hook that will check QMM and send an email if
*-- there are too many waiting messages.
If VarType(tcUser) = "C" and Upper(Alltrim(tcUser)) = "TEST"
	llCheckQMM = .t.
EndIf

If !Empty(lcBootstrap) && Modify the following code for your environment, if needed.
	lcUser = "QMM"
	lcPassword = "ZYA817AKJ"
	llShowUpdateDialog = .f.
	llLogBootup = !llCheckQmm
	Do (lcBootstrap) with lcUser, lcPassword, llShowUpdateDialog, llLogBootup
Endif

LoadLibraries()

lcAppName = "QMM"
goApp.cAppName = lcAppName
goApp.oConfig.app_name = lcAppName

If llCheckQmm
	loCheckMessageQueue = CreateWWBO("CheckMessageQueue")
	llSendEmailIfQmmNotWorking = .t.
	loCheckMessageQueue.Check("", llSendEmailIfQmmNotWorking)

	goApp.oTracker.Track(loCheckMessageQueue, ;
						 "Checking QMM message count.", ;
						 Transform(loCheckMessageQueue.nWaitingMessages))
	If goApp.lExe
		Quit
	EndIf

	Try
		Cd 'H:\Work\lm5_qmm' && Modify for your environment
	Catch
	EndTry

	Return
	
EndIf

poConfig = CreateObject("ImageServerINI")
poConfig.Load()

lcConnectionString = goApp.oConfig.Connstring && See note above about goApp

*-- Create Queue Monitor UI form
loForm = CreateObject("QueueMonitorForm")

*-- Create and set the MessageProcessor object use by the AsyncMessaging class.
*-- You processor class needs to handle all possible message types.
If Type("goApp") = "O"
	lcMessageProcessorClass = "lmMessageProcessor"
	loMessageProcessor = CreateWWBO(lcMessageProcessorClass)
Else
	lcMessageProcessorClass = "message_processor_example"
	loMessageProcessor = CreateObject(lcMessageProcessorClass)
Endif

If Type("loMessageProcessor") != "O"
	MessageBox("Error creating oMessageProcessor object from class " + lcMessageProcessorClass)
	Return .f.
Endif

*-- Assign certain objects
loForm.oAsync.oSql = goApp.oSql && See note above about goApp
loForm.oMessageProcessor = loMessageProcessor

loForm.StartPolling(Val(poConfig.cPollingInterval)) 
goApp.LogInfo('VFP QMM App instance started.')

*-- Display connection string on form label
lcServer = Getwordnum(lcConnectionString, 2, ';')
lcDatabase = Getwordnum(lcConnectionString, 3, ';')
lcUserName = Getwordnum(lcConnectionString, 4, ';')
lcPassWord = "pwd=XXXXXXX;"
lcLabelCaption = lcServer + ";" + lcDatabase + ";" + lcUserName + ";" + lcPassWord
loForm.lblConnectionString.Caption = Strtran(lcLabelCaption, ";", "; ", 1, 9)

goApp.oConfig.app_name = "Labor Management - Message Queue Monitor" && Modify for your environment. See note above about goApp

*-- Display form
loForm.Show()

Read Events

*---------------------------------------------------------------------------------------
Procedure LoadLibraries()

	*-- This code uses a global app object to call Set Procedure and Set Classlib
	*-- If your environment does not have goApp, then modify these next lines to use conventional 
	*-- FoxPro code to activate these requires assets.
	goApp.Require(SOURCE_ROOT + "Classes\wwAsyncServer.vcx")
	goApp.Require(SOURCE_ROOT + "Classes\QueueMonitor.vcx")
	goApp.Require("WatermarkListener.prg")
	goApp.Require("FoxBarcode.prg")
			
	*-- West Wind classes used by the app
	goApp.Require("wwSmtp.prg")
	goApp.Require("wwPDF.prg")		


Endproc

 
 
*=======================================================================================
Define Class ImageServerINI As wwConfig

	cMode = "INI"
	cFilename = "vfpmpwfqmm.ini"
	cSQLConnectString = ''
	cMailCC = ''
	cMailFrom = ''
	cMailFromEmail = ''
	cMailServer = ''

	cOrderLineImagePath = ''
	cPollingInterval = '1000'

Enddefine  