/****** Object:  Table [dbo].[QueueMessageItems]    Script Date: 7/5/2016 3:37:02 PM ******/

/* See https://github.com/RickStrahl/Westwind.QueueMessageManager */

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[QueueMessageItems](
	[Id] [nvarchar](50) NOT NULL CONSTRAINT [DF_QueueMessageItems_Id]  DEFAULT (CONVERT([nvarchar](36),newid())),
	[QueueName] [nvarchar](40) NULL,
	[Status] [nvarchar](50) NULL,
	[Action] [nvarchar](80) NULL,
	[Submitted] [datetime] NOT NULL,
	[Started] [datetime] NULL,
	[Completed] [datetime] NULL,
	[IsComplete] [bit] NOT NULL CONSTRAINT [DF_QueueMessageItems_IsComplete]  DEFAULT ((0)),
	[IsCancelled] [bit] NOT NULL CONSTRAINT [DF_QueueMessageItems_IsCancelled]  DEFAULT ((0)),
	[Expire] [int] NOT NULL CONSTRAINT [DF_QueueMessageItems_Expire]  DEFAULT ((0)),
	[Message] [nvarchar](max) NULL,
	[TextInput] [nvarchar](max) NULL,
	[TextResult] [nvarchar](max) NULL,
	[NumberResult] [decimal](18, 2) NOT NULL CONSTRAINT [DF_QueueMessageItems_NumberResult]  DEFAULT ((0)),
	[Data] [nvarchar](max) NULL,
	[BinData] [varbinary](max) NULL,
	[Xml] [nvarchar](max) NULL,
	[Json] [nvarchar](max) NULL,
	[PercentComplete] [int] NOT NULL CONSTRAINT [DF_QueueMessageItems_PercentComplete]  DEFAULT ((0)),
	[XmlProperties] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.QueueMessageItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
