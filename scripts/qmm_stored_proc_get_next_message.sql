/* See https://github.com/RickStrahl/Westwind.QueueMessageManager */


GO
/****** Object:  StoredProcedure [dbo].[qmm_GetNextQueueMessageItem]    Script Date: 7/5/2016 3:40:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE  [dbo].[qmm_GetNextQueueMessageItem]
  @QueueName nvarchar(80), @Count int = 1
 AS
 
 --SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
 --BEGIN TRANSACTION 
   UPDATE QueueMessageItems
          SET [Started] = GetUtcDate(), [Status] = 'Started'
		  OUTPUT INSERTED.*		  
          WHERE Id in (
			  SELECT TOP(@Count)
				   Id FROM QueueMessageItems WITH (UPDLOCK)	   
				   WHERE [QueueName] = @QueueName AND
                         [Started] is null
                   ORDER BY Id         						 
		  )
--COMMIT TRANSACTION