Local loMessages as "lmMessages" of "lmMessages.prg"
Local loTimeClock as 'TimeClock' of 'TimeClock.prg'

loTimeClock = CreateWWBO('TimeClock')

lnScheduleItemId = 81690

loTimeClock.Execute("Update ScheduleItems set Status = '' where id = " + Transform(lnScheduleItemId))

loMessages = CreateWWBO('lmMessages')

loMessages.MarkScheduleTaskComplete(lnScheduleItemID)


Return 

*== Test Code to method on BO which handles above message =====================================================================================

Local loMessages

loMessages = CreateWWBO('lmMessages')

lnScheduleItemId = 81690
ldCompletionDateTime = .null.
lnCompletedById = .null.

loMessages.MarkScheduleTaskComplete(lnScheduleItemId, ldCompletionDateTime, lnCompletedById)

loMessages.MarkScheduleTaskComplete(lnScheduleItemId, .null., .null.)


? "Done."


