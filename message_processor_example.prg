#Include "Messaging.h"

*-- This is an example Message Processor class to show you how can process messages.

*-- The ProcessMessage() on this class is called by the AsyncMessaging class when a new Message is found
*-- in the QueueMessageItems database table.

*-- The ProcessMessage() method should return .t. or .f. to indicate if the Message was processed successfully.

*-- The AsyncMessaging class will handle updating the the status of the Message based on the return result from
*-- the ProcessMessage() method.

Define Class Message_Processor_Example as Custom

	cMessage = ""
	lResult = .f.
	cErrorMessage = ""
	
	*---------------------------------------------------------------------------------------
	Procedure ProcessMessage(toAsync)

		Local lcAction, lcMessage, llError, loMessage
		Local lcErrorMessage, ldCompletionDateTime, llResult, llReturn, lnScheduleItemID, loSMTP
		Local loScheduleHelper

		llError = .f.
		lcMessage = ""
		lcErrorMessage = ""
		loMessage = toAsync.oMessage
		*#Alias toAsync = {AsyncMessaging, AsyncMessaging.prg}

		lcAction = Alltrim(Upper(loMessage.Action))

		Do Case

			*---------------------------------------------------------------------------------------
			Case BeginsWith(lcAction, "SendEmail", .t.)
				Try
					loSMTP = goApp.GetAdminEmailer()
					loSMTP.cSubject = toAsync.GetProperty("cSubject")
					loSMTP.cRecipient = toAsync.GetProperty("cRecipient")
					loSMTP.cMessage = toAsync.oMessage.TextInput
					llResult = loSMTP.SendMail()					
					lcMessage = "Email message sent."
				Catch
					lcErrorMessage = "Error sending email."
					llError = .t.
				EndTry
				
			*---------------------------------------------------------------------------------------
			Case lcAction = Upper(QMM_MarkScheduleTaskComplete)
				Try
					lnScheduleItemID = toAsync.GetProperty("ScheduleItem_id")
					ldCompletionDateTime = toAsync.GetProperty("CompletionDateTime")
	
					loScheduleHelper = CreateWWBO("ScheduleHelper")
					llReturn = loScheduleHelper.MarkScheduleTaskAsComplete(lnScheduleItemID, ldCompletionDateTime)
					
					If llReturn
						lcMessage = "Schedule task marked as Complete."
					Else
						lcErrorMessage = "Error marking Schedule task Complete."
						llError = .t.
					Endif
				Catch
					lcErrorMessage = "Error marking schedule task Complete."
					llError = .t.
				EndTry
				
			*---------------------------------------------------------------------------------------
			Case lcAction = Upper(QMM_PrintShopOrder)
			
				Try
					*-- Create the Business Object that will do all the work
					loJob = CreateWWBO('Job')
					
					*-- Pull values from the Message
					lnJobID = toAsync.GetProperty("JobID")
					lcReportName = toAsync.GetProperty("ReportName")
					lcMode = toAsync.GetProperty("Mode")
					llPreview = "PREVIEW" $ Upper(lcMode)

					*-- Call the method to do the work requested
					llReturn = loJob.PrintShopOrder(lnJobID, lcReportName, llPreview)
					
					If !llReturn
						lcErrorMessage = loJob.GetErrorText()
						llError = .t.
					EndIf
					
				Catch
					lcErrorMessage = "Error printing Shop Order."
					llError = .t.
				EndTry

			Otherwise
				lcErrorMessage = "Error: Not a supported action: " + Alltrim(loMessage.Action)
				llError = .t.
		Endcase

		This.cMessage = lcMessage
		This.cErrorMessage = lcErrorMessage
		This.lResult = !llError

		Return lResult

	EndProc
	
EndDefine
  