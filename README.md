# Message Queue / Async Processing for Visual Foxpro #
See this blog post for details:
 
http://mattslay.com/message-queue-solution-for-visual-foxpro-applications
 

Using some helpful classes from the West Wind Web Connection framework (or West Wind Client Tools package) we can implement a robust Message Queue solution in Visual FoxPro.  All it takes is two simple steps:

**Step 1 – Submit a “Message” from your app as needed:**  Create and submit a simple Message record to the QueueMessageItems database table which has the name of the Process or Action that you want to execute, along with any required parameters, values, settings, user selections, other other criteria required to complete the task.  Boom… in a blink the local user workflow is finished and they think your system is the fastest thing in the world, because now their computer is free in a few milliseconds and they can resume other tasks.

**Step 2 – Process the “Message” using the Message Manager class:** You will setup a computer to run the Message Queue Manager, (somewhere the hall on a server server or other workstation) that can access the database, network drives, printers, internet, make API calls, etc needed to complete the task.  Once the Message (i.e. task) is picked up by the Processor it is marked as Started, and when all the processing is finished the record is marked as Complete. Along the way you can send other other messages, emails, SMS text messages, reject messages, etc.

## Setup
There are a few basic step to implementing this: 
* The AsyncMessaging class (see AsyncMessaging.prg code file.) This class handles all the message submissions and reading/upating messages.

* The UI form is used to initiate and monitor message polling and processing. See QueueMonitor.vcx (see QueueMonitorForm class) and the base class library wwAsyncServer.vcx (see ServerPollForm class)

* You will need to create a MessageProcessor class to handle processing of each message passed to it from the timer event on the QueueMonitor UI form. You can see this setup in the Startup_qmm.prg code. 

```csharp
loForm = CreateObject("QueueMonitorForm") && Create the UI form
loMessageProcessor = CreateObject("Message_Processor_Example") && Create the Message Process object
loForm.oMessageProcessor = loMessageProcessor && Assign to form property
```

* Finally, see Startup_qmm.prg for the main launch code. You will need to customize certain parts of it for your unique environment. In the code, you will see that after an instance of the QueueMonitor form is created, and the oMessageProcessor property is set, the polling process is then started with this code:

```csharp
loForm.StartPolling()
```


**See additional dependencies below screenshot.**



![message_queue_monitor_form.png](https://bitbucket.org/repo/M8rXyn/images/3870072060-message_queue_monitor_form.png)

# Dependencies

Use of this package requires that you also download the West Wind Web Connection (https://west-wind.com/webconnection) or West Wind Client Tools library (https://west-wind.com/WestwindClientTools.aspx) Trial versions are available but do require a license for production use.

You can read Rick Strahl’s extensive whitepaper on his wwAsync class (https://west-wind.com/presentations/wwAsyncWebRequest/wwAsyncWebRequest.htm). Note: the whitepaper is kind of old, and the focus there is on how his Web Connection framework uses the wwAsync class to handle back end processing in web apps, but using this on a LAN or desktop/Client Application works exactly the same way.) Documentation of each method and property can be found here: http://west-wind.com/webconnection/docs/_0ci0pqwxq.htm
